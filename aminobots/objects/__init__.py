"""
aminobots.objects
-----------------

Response objects for the Amino API.

:copyright: (c) 2023 ViktorSky
:license: MIT, see LICENSE for more details.

"""

from .account import *
from .chatmessage import *
from .community import *
from .communitylist import *
from .currentuserinfo import *
from .invitation import *
from .linkinfov2 import *
from .paging import *
from .payload import *
from .userinfoincommunities import *
from .userprofile import *
from .userprofilelist import *
